<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Validation\Validator;

class Controller extends BaseController
{

    public function success($data = [], $status = 200)
    {
        $data = $data ?: [];
        $data['status'] = 'true';

        foreach($data as $key => $value) {
            if ($value instanceof Arrayable) {
                $data[$key] = $value->toArray();
            }
        }

        return response()->json($data, $status);
    }

    public function error($messages = [], $status = 422)
    {
        $data = ['status' => 'false', 'messages' => $messages ?: []];
        return response()->json($data, $status);
    }

    protected function formatErrors(Validator $validator, $status = 422) {
        $errors = $validator->errors()->getMessages();
        $transformed = [];
    
        foreach ($errors as $field => $messages) {
          $transformed[$field] = $messages[0];
        }
        
        $data = ['status' => 'false', 'error' => $transformed];

        return response()->json($data, $status);
        
      }
}
