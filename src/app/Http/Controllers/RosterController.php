<?php

namespace App\Http\Controllers;

use App\Services\RosterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RosterController extends Controller
{
    
    protected $rosterService;
    
    public function __construct(RosterService $rosterService)
    {
        $this->rosterService = $rosterService;
    }

    public function parse(Request $request){

        $validator = Validator::make($request->all(), [
            'url' => 'required|url'
        ]);

        if ($validator->fails()) {
            return $this->formatErrors($validator);
        }

        try {

            $results = $this->rosterService->parse($request->get('url'));
        
        } catch (\Exception $e) {
            return $this->error(['error' => $e->getMessage()]);
        }
       
        return $this->success(['data' => $results, 'message' => "url parsed successfully"]);
    }

    public function filter(Request $request){

        $validator = Validator::make($request->all(), [
            'event' => 'sometimes|required|string|max:3',
            'location' => 'sometimes|required|string',
            'date_from' => 'sometimes|date_format:Y-m-d|required',
            'date_to' => 'sometimes|date_format:Y-m-d|required'
        ]);

        if ($validator->fails()) {
            return $this->formatErrors($validator);
        }

        $filters = request()->all();
        
        try {

            $query = $this->rosterService->filter($filters);
        
        } catch (\Exception $e) {
            return $this->error(['error' => $e->getMessage()]);
        }

        if($query->get()->count()){
        
            return $this->success(['data' => $query->get(), 'message' => "results found successfully"]);

        }else{
            
            return $this->success(['data' => [], 'message' => "no results found"]);  
        }

    }

    public function upload(Request $request){
        
        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:pdf,xls,html,txt,ics'
        ]);

        if ($validator->fails()) {
            return $this->formatErrors($validator);
        }

        try {

            $file = $this->rosterService->upload($request->file('file'));
        
        } catch (\Exception $e) {
            return $this->error(['error' => $e->getMessage()]);
        }

        return $this->success(['data' => ['file' => $file], 'message' => "file uploaded successfully"]);

    }
}
