<?php
 
namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
 
class Roster extends Model
{
    protected $table = 'roster_buster';
    protected $fillable = [
        'Date', 'Rev', 'DC', 'CI_L', 'CI_Z', 'CO_L', 'CO_Z', 'Activity', 'Remark', 'From', 'STD_L', 'STD_Z ', 'To', 'STA_L', 'STA_Z', 'AC_Hotel', 'BLH', 'Flight_Time', 'Night_Time', 'Dur', 'Ext', 'Pax_booked', 'ACReg', 'Event', 'Event_Date'];
}