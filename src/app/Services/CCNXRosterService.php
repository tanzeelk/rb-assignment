<?php

namespace App\Services;

use App\Services\RosterService;
use App\Models\Roster;
use Carbon\Carbon;
use Goutte\Client;

class CCNXRosterService extends RosterService
{
    
    private $roster;
    private $client;

    public function __construct(Roster $roster, Client $client)
    {
        $this->roster = $roster;
        $this->client = $client;
    }
    
    public function extractTable($page){
    
        $table = $page->filter('#ctl00_Main_activityGrid')->filter('tr')->each(function ($tr, $i) {
            return $tr->filter('td')->each(function ($td, $i) {
                return trim($td->text());
            });
        });

        return $table;
    }

    public function loadData($page, $table){

        // array to return results.
        $results = array();

        // Dom parsed to extract Month and Year.
        $printOnly = $page->filter('.printOnly')->children()->text();
        $pattern = "/[A-Z][A-Z][A-Z][0-9][0-9]/i";
        preg_match($pattern, $printOnly, $date_match);

        // Removed header from the extracted table.
        array_shift($table);

        $date="";
        $prev_date=""; // For storing date of previous row.  

        foreach ($table as $row){
            
            if($row[1]==""){
                $date = $prev_date;
            }else{
                $date = $row[1];
                $prev_date = $row[1];
            }

            // Method called to  get event type.
            $event = $this->getEvent($row);
            
            $new_date = $date . $date_match[0];
            $event_date = Carbon::createFromFormat('D dMy',  $new_date);

            $roster = new Roster();
            
            $roster['Date'] = $date;
            $roster['Rev'] = $row[2];
            $roster['DC'] = $row[3];
            $roster['CI_L'] = $row[4];
            $roster['CI_Z'] = $row[5];
            $roster['CO_L'] = $row[6];
            $roster['CO_Z'] = $row[7];
            $roster['Activity'] = $row[8];
            $roster['Remark'] = $row[9];
            $roster['From'] = $row[11];
            $roster['STD_L'] = $row[12];
            $roster['STD_Z '] = $row[13];
            $roster['To'] = $row[15];
            $roster['STA_L'] = $row[16];
            $roster['STA_Z'] = $row[17];
            $roster['AC_Hotel'] = $row[19];
            $roster['BLH'] = $row[20];
            $roster['Flight_Time'] = $row[21];
            $roster['Night_Time'] = $row[22];
            $roster['Dur'] = $row[23];
            $roster['Ext'] = $row[24];
            $roster['Pax_booked'] = $row[26];
            $roster['ACReg'] = $row[27];
            $roster['Event'] = $event;
            $roster['Event_Date'] = $event_date->toDateString();
            
            $roster->save();
            
            array_push($results, $roster);
        }

        return $results;
        
    }

    public function parse($url){
    
        $page = $this->client->request('GET', $url);
        
        // Using Goutte to extract roster table.
        $table = $this->extractTable($page);
        
        // Load data into database.
        $results = $this->loadData($page, $table);
        
        return $results;
    }

    public function getEvent($row){

        $pattern = "/[A-Z][A-Z][0-9]+/i";

        if($row[8] == "SBY"){
            $event = "SBY";
        }elseif($row[8] == "OFF"){
            $event = "DO";
        }elseif(strlen($row[5]) > 3 && preg_match($pattern, $row[8]) ){ 
            $event = "CI";
        }elseif(strlen($row[7]) > 3 && preg_match($pattern, $row[8]) ){
            $event = "CO";
        }elseif( preg_match($pattern, $row[8]) ){
            $event = "FLT";
        }else{
            $event = "UNK";
        }

        return $event;
    }

    public function filter($filters){
        
        $query = $this->roster->newQuery();

        // Search for events based on their type.
        if (isset($filters['event'])) {
            $query->where('Event', $filters['event']);
        }
        
        // Search for events starting from location.
        if (isset($filters['location'])) {
            $query->where('From', $filters['location']);
        }

        // Search for events between two dates.
        if (isset($filters['date_from']) && isset($filters['date_to'])) {
            $query->whereBetween('Event_Date', [$filters['date_from'], $filters['date_to']]);
        }

        // Search for events within a week starting from given date.
        if (isset($filters['date_from']) && !isset($filters['date_to'])) {
            
            $date_week = Carbon::createFromFormat('Y-m-d',  $filters['date_from'])->addDays(7);
            $query->whereBetween('Event_Date', [$filters['date_from'], $date_week->toDateString()]);
        }

        return $query;

    }

    public function upload($file){

        $filename = time().'_'.$file->getClientOriginalName();
        $destinationPath = 'uploads';

        // File uploaded in uploads folder
        $file->move($destinationPath, $filename);
        
        return $filename;     
    }
}