<?php

namespace App\Providers;

use App\Services\RosterService;
use App\Services\CCNXRosterService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /**
         * Based on airline parameter respective roster service can be resolved.
         */
        //$className = $this->resolveClassName(Request::get('airline')));
        
        $this->app->bind(RosterService::class, CCNXRosterService::class);
    }
}
