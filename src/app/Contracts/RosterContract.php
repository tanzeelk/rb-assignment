<?php

namespace App\Contracts;

interface RosterContract
{
    public function parse($url);

    public function filter($filters);

    public function upload($file);
}