<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response;

class ApiTest extends TestCase
{
    public function testParseApi()
    {   
        $data = [
            'url' => 'http://139.59.41.85/roster.html'
        ];

        $response = $this->call('POST', '/api/parse', $data);

        $this->assertEquals(200, $response->status());
    }

    public function testFilterApi()
    {   
        $data = [
            'event' => 'FLT',
            'location' => 'KRP'
        ];

        $response = $this->call('GET', '/api/filter', $data);

        $this->assertEquals(200, $response->status());
    }
}
