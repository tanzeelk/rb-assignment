<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Illuminate\Http\Response;
use Goutte\Client;
use App\Services\CCNXRosterService;
use App\Models\Roster;

class ParseTest extends TestCase
{
    public function testParseUrl()
    {   
        $url = 'http://139.59.41.85/roster.html';
        
        $client = new Client();
        $roster = new Roster();
        $rosterService = new CCNXRosterService($roster, $client);

        $results = $rosterService->parse($url);

        $this->seeInDatabase('roster_buster', ['Event' => 'UNK']);

        //$this->assertEquals(200, $results->status());
    }

    public function testExtractTable()
    {   
        $url = 'http://139.59.41.85/roster.html';
        
        $client = new Client();
        $roster = new Roster();
        $rosterService = new CCNXRosterService($roster, $client);

        $page = $client->request('GET', $url);

        $results = $rosterService->extractTable($page);

        if(count($results))
            $this->assertTrue(true);
    }

    public function testEventType()
    {   
        $url = 'http://139.59.41.85/roster.html';
        
        $client = new Client();
        $roster = new Roster();
        $rosterService = new CCNXRosterService($roster, $client);

        $page = $client->request('GET', $url);

        $results = $rosterService->extractTable($page);

        $event = $rosterService->getEvent($results[1]);

        $types = array("CI", "FLT", "CO", "SBY", "DO", "UNK");
        
        if (in_array($event, $types)) {
            $this->assertTrue(true);
        }
    }

    public function testEventTypeManual()
    {   
        $client = new Client();
        $roster = new Roster();
        $rosterService = new CCNXRosterService($roster, $client);

        $row = [
            5 => "0745",
            7 => " ",
            8 => "D77",
        ];

        $event = $rosterService->getEvent($row);

        $types = array("CI", "FLT", "CO", "SBY", "DO", "UNK");
        
        if (in_array($event, $types)) {
            $this->assertTrue(true);
        }
    }
}
