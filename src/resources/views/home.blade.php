@extends('layouts.master')
 
@section('content')
    <div class="wrap">
        <div class="Background">
            <canvas class="Background-canvas"></canvas>
        </div>
        <div class="TextContainer">
            <div class="Text">
                <canvas class="Text-title">API Guide Roster Buster</canvas>
                <canvas class="Text-titleMedium" data-breakpoint="1040">API Guide Roster Buster</canvas>
                <canvas class="Text-titleSmall" data-breakpoint="720">API Guide Roster Buster</canvas>
                <!-- <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p> -->
                <h4>API Endpoints</h4>
                <ul>
                    <li style="margin: 30px 0"><span style="color:#FF6938;">POST</span> <span>/api/parse</span> 
                        <h6 style="margin: 10px 0">Parameters</h6>
                        <ul style="font-size: 16px;">
                            <li><em>url - (Required) HTML roster link from CCNX system</em></li>
                        </ul>
                    </li>
                    <li style="margin: 30px 0"><span style="color:#FF6938;">GET</span> <span>/api/filter</span>
                        <h6 style="margin: 10px 0">Parameters</h6>
                        <ul style="font-size: 16px;">
                            <li><em>event - (Optional) To filter events of type - FLT, CI, CO, DO, SBY, UNK</em></li>
                            <li><em>location - (Optional) To filter events that start from location.</em></li>
                            <li><em>date_from - (Optional) To filter events between start date and end date. If used without end date - will return result within one week of start date.</em></li>
                            <li><em>date_to - (Optional) To filter events between start date and end date</em></li>
                        </ul>
                    </li>
                    <li style="margin: 30px 0"><span style="color:#FF6938;">POST</span> <span>/api/upload</span>
                        <h6 style="margin: 10px 0">Parameters</h6>
                        <ul style="font-size: 16px;">
                            <li><em>file - (Required) Roster file to be uploaded in these formats - pdf,xls,html,txt,ics</em></li>
                        </ul>
                    </li>
                </ul>
                <a href="https://www.getpostman.com/collections/bf4ba111d631ecc2dab0" target="_blank">View collection in Postman</a>
            </div>
        </div>
    </div>
@stop