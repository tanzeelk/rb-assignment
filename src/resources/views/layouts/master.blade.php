<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Roster Buster Assignment</title>
        <link rel="stylesheet" type="text/css" href="{{ url('/css/normalize.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/demo.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('/css/flight.css') }}" />
        <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container">
            @yield('content')
        </div>
        <script src="{{ url('/js/flight.js') }}"></script>
    </body>
</html>