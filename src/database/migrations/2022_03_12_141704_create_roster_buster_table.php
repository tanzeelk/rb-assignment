<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRosterBusterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roster_buster', function (Blueprint $table) {
            $table->id();
            $table->string('Date')->nullable();
            $table->string('Rev')->nullable();
            $table->string('DC')->nullable();
            $table->string('CI_L')->nullable();
            $table->string('CI_Z')->nullable();
            $table->string('CO_L')->nullable();
            $table->string('CO_Z')->nullable();
            $table->string('Activity')->nullable();
            $table->string('Remark')->nullable();
            $table->string('From')->nullable();
            $table->string('STD_L')->nullable();
            $table->string('STD_Z ')->nullable();
            $table->string('To')->nullable();
            $table->string('STA_L')->nullable();
            $table->string('STA_Z')->nullable();
            $table->string('AC_Hotel')->nullable();
            $table->string('BLH')->nullable();
            $table->string('Flight_Time')->nullable();
            $table->string('Night_Time')->nullable();
            $table->string('Dur')->nullable();
            $table->string('Ext')->nullable();
            $table->string('Pax_booked')->nullable();
            $table->string('ACReg')->nullable();
            $table->string('Event')->nullable();
            $table->string('Event_Date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roster_buster');
    }
}
